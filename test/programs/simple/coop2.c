// This file is part of frama-c-sv,
// a wrapper around Frama-C that makes it possible
// to execute Frama-C as part of the Competition on Software Verification:
// https://gitlab.com/sosy-lab/software/frama-c-sv
//
// SPDX-FileCopyrightText: 2020-2021 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

extern int __VERIFIER_nondet_int();
void reach_error(){}

void __VERIFIER_assert(int cond) {
  if (!cond) {
    reach_error();
  }
}

int main() {
  int x = 0;
  int y = 0;
  //if (!(y == x)) reach_error();
  //@ assert y == x;
  while (__VERIFIER_nondet_int()) {
  if (!(y == x)) reach_error();
    x++;
    y++;
    //@ assert y==x;
    //if (!(y == x)) reach_error();
  }
  __VERIFIER_assert(x==y);
}

