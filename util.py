# This file is part of frama-c-sv,
# a wrapper around Frama-C that makes it possible
# to execute Frama-C as part of the Competition on Software Verification:
# https://gitlab.com/sosy-lab/software/frama-c-sv
#
# SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from enum import Enum


class Property(Enum):
    UNREACH_CALL = "unreach-call"
    NO_OVERFLOW = "no-overflow"
    MEMORY_SAFETY = "valid-memsafety"
    TERMINATION = "termination"
    VALID_ACSL = "valid-acsl"

    @staticmethod
    def from_text(property_text):
        if "overflow" in property_text:
            return Property.NO_OVERFLOW
        elif "reach_error" in property_text:
            return Property.UNREACH_CALL
        elif "valid-free" in property_text:
            return Property.MEMORY_SAFETY
        elif "termination" in property_text:
            return Property.TERMINATION
        elif "valid-acsl" in property_text:
            return Property.VALID_ACSL
        else:
            raise ValueError("unknown property %s" % property_text)


try:
    import pycparser
    from pycparser.plyparser import ParseError

    class FuncCallVisitor(pycparser.c_ast.NodeVisitor):
        def __init__(self):
            self.called = set()

        def visit_FuncCall(  # noqa N802 function must have this name to be called
            self, node
        ):
            self.called.add(node.name.name)
            # Visit args in case they contain more function calls
            if node.args:
                self.visit(node.args)

    class FuncDefVisitor(pycparser.c_ast.NodeVisitor):
        def __init__(self):
            self.func_defs = {}

        def visit_FuncDef(  # noqa N802 function must have this name to be called
            self, node
        ):
            call_visitor = FuncCallVisitor()
            call_visitor.visit(node)
            self.func_defs[node.decl.name] = call_visitor.called

    def find_recursive(funcs):
        changed = funcs.keys()
        while changed:
            updated = changed
            changed = set()
            for func in updated:
                for caller in [f for f in funcs if func in funcs[f]]:
                    called = funcs[caller].union(funcs[func])
                    if len(called) > len(funcs[caller]):
                        funcs[caller] = called
                        changed.add(caller)
        return {func for func in funcs if func in funcs[func]}

    def get_inline_functions(program):
        try:
            try:
                from pycparserext.ext_c_parser import GnuCParser

                parser = GnuCParser()
            except ImportError:
                parser = None
            ast = pycparser.parse_file(program, use_cpp=True, parser=parser)
            visitor = FuncDefVisitor()
            visitor.visit(ast)
            inline_functions = visitor.func_defs.keys()
            inline_functions -= find_recursive(visitor.func_defs).union(
                {"main", "reach_error"}
            )
            return inline_functions
        except (ParseError, NotImplementedError):
            return None


except ImportError:

    def get_inline_functions(_):
        return None
