# This file is part of frama-c-sv,
# a wrapper around Frama-C that makes it possible
# to execute Frama-C as part of the Competition on Software Verification:
# https://gitlab.com/sosy-lab/software/frama-c-sv
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

.PHONY: tests,test,bench,%.cx
.DEFAULT_GOAL := tests

framac: Vagrantfile
	vagrant up
	vagrant destroy -f

tests:
	echo "\e[1mrunning black formatter:\e[21m"
	black . --check --diff
	echo "\e[1mrunning nose tests:\e[21m"
	nosetests --verbose --with-coverage --cover-package . test/
	echo "\e[1mrunning flake8:\e[21m"
	flake8
	echo "\e[1mrunning pytype:\e[21m"
	pytype -k -n frama-c-sv.py

reuse:
	reuse lint

bench:
	benchexec test/test-sets/tests.xml
%.cr: %.c
	python3 frama-c-sv.py --property test/properties/unreach-call.prp --program $<
%.cm: %.c
	python3 frama-c-sv.py --property test/properties/valid-memsafety.prp --program $<
%.co: %.c
	python3 frama-c-sv.py --property test/properties/no-overflow.prp --program $<
test: test/false_reach_error2.cr
