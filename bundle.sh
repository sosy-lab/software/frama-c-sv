#!/bin/bash
# This file is part of frama-c-sv,
# a wrapper around Frama-C that makes it possible
# to execute Frama-C as part of the Competition on Software Verification:
# https://gitlab.com/sosy-lab/software/frama-c-sv
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0
DIRNAME="frama-c-sv"
mkdir $DIRNAME
cp frama-c-sv.py $DIRNAME
cp interpreter.py $DIRNAME
cp util.py $DIRNAME
cp -r LICENSES $DIRNAME/LICENSES
cp -r LICENSE $DIRNAME/LICENSE
cp -r README.md $DIRNAME/README.md
cp -r config $DIRNAME/config
cp harness.c $DIRNAME
cp template.graphml $DIRNAME
if [[ -e "framac" ]]
then
  cp -r framac $DIRNAME/framac
else
  echo "WARNING: no framac to bundle with frama-c-sv was found"
  echo "         (you might want to run `make framac`!)"
fi
rm -rf frama-c-sv.zip
zip -r frama-c-sv.zip frama-c-sv
rm -rf frama-c-sv
