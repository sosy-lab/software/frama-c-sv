// This file is part of frama-c-sv,
// a wrapper around Frama-C that makes it possible
// to execute Frama-C as part of the Competition on Software Verification:
// https://gitlab.com/sosy-lab/software/frama-c-sv
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0
int main() {
  int * p = 27;
  *p = 42;
  return 0;
}
