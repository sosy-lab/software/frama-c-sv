# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.box = "ubuntu/jammy64"

  name = "framac"
  ncores = "4"

  config.vm.provider "virtualbox" do |vb|
    vb.gui = false
    vb.cpus = ncores
    vb.memory = "4000" # MB
    vb.name = name
  end

  config.vm.provision "shell", inline: <<-SHELL
    apt-get update
    apt-get -y install ocaml opam autoconf zip
  SHELL

  config.vm.provision "repository", type: "shell", privileged: false, inline: <<-SHELL
    git clone --branch 25.0 --depth 1 https://git.frama-c.com/pub/frama-c.git
    cd frama-c
    mkdir /tmp/framac
    HOME=/tmp/framac
    opam init -n
    opam switch create framac ocaml.4.13.1 
    opam switch framac
    opam install -j#{ncores} --confirm-level=unsafe-yes --deps-only frama-c
    # opam install -j#{ncores} -y why3-coq # uncomment this in case you need why3-coq, but it increases the size by factor 2
    eval $(opam env)
    autoconf
    ./configure --prefix=/tmp/framac
    make -j#{ncores}
    make install
    rm -rf /tmp/framac/.opam/framac/.opam-switch
    rm -rf /tmp/framac/.opam/download-cache
    rm -rf /tmp/framac/.opam/repo
    rm -rf /tmp/framac/.cache
    rm -rf /tmp/framac/bin/{e-acsl-gcc.sh,frama-c-config,frama-c-gui,frama-c-gui.byte,frama-c-script,frama-c.byte,ptests.opt}
    rm -rf .opam/framac/doc
    rm -rf /vagrant/framac
    mv /tmp/framac /vagrant/framac
  SHELL

end
