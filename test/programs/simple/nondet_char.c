// This file is part of frama-c-sv,
// a wrapper around Frama-C that makes it possible
// to execute Frama-C as part of the Competition on Software Verification:
// https://gitlab.com/sosy-lab/software/frama-c-sv
//
// SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

void reach_error(){};
extern unsigned char __VERIFIER_nondet_uchar(void);
int main() {
  signed int i = -256;
  unsigned char c = __VERIFIER_nondet_uchar();
  if (i+c>=0)  {
    reach_error();
  
  }
  i = -128;
  signed char c2 = __VERIFIER_nondet_uchar();
  if (i+c2>=0) {
    reach_error();
  }
}
