#!/usr/bin/env python3
# This file is part of frama-c-sv,
# a wrapper around Frama-C that makes it possible
# to execute Frama-C as part of the Competition on Software Verification:
# https://gitlab.com/sosy-lab/software/frama-c-sv
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import sys

sys.dont_write_bytecode = True
import hashlib
import pathlib
import subprocess  # noqa: S404 our intention IS to run frama-c via subprocess
import re
import argparse
import getpass
import logging
import os
import stat
import yaml

from datetime import datetime

from interpreter import ReportInterpreter, RESULT_TRUE_PROP, RESULT_UNKNOWN
from util import Property, get_inline_functions


OUT_DIR = "output"

VERSIONSTRING = "frama-c-sv version 0.4.0"

FRAMACPATH = "frama-c"

FIXEDPATH = "/tmp/framac"


def parse_args():
    parser = argparse.ArgumentParser(
        description="Wrapper around Frama-C for use in SV-COMP.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "--property",
        type=str,
        help="File containing the property that shall be verified.",
    )
    parser.add_argument("--program", type=str, help="Input program for the analysis.")
    parser.add_argument(
        "--datamodel",
        type=str,
        default="ILP32",
        help="The data model that shall be assumed when analyzing the input program. Either ILP32 or LP64.",
    )
    parser.add_argument(
        "--version", action="store_true", help="Output the version of Frama-C-SV"
    )
    parser.add_argument(
        "--reach-as-overflow",
        dest="reach_as_overflow",
        action="store_true",
        help="Whether to encode calls to reach_error as overflows.",
    )
    parser.add_argument(
        "--reach-as-acsl",
        dest="reach_as_acsl",
        action="store_true",
        help="Interpret the property unreach-call such as to just check all existing ACSL annotations. "
        + "This will not automatically instrument the reach_error call!",
    )
    parser.add_argument(
        "--config",
        type=config_file,
        default=resource("./config/default.yml"),
        help="The config file to be used.",
    )
    parser.add_argument(
        "--allow-unsafe",
        dest="unsafe",
        action="store_true",
        help="Allow unsafe directory mode for bundled frama-c distribution",
    )
    parser.add_argument(
        "additional",
        nargs="*",
        metavar="ARG",
        help='command line to run (prefix with "--" '
        + "to ensure all arguments are treated correctly)",
    )

    args, unknown = parser.parse_known_args()

    if args.version:
        print(VERSIONSTRING)  # noqa T001
        sys.exit()
    else:
        logging.getLogger("WRAPPER").info(f"Running {VERSIONSTRING}")

    if len(unknown) == 1:
        try:
            config_name = unknown[0][2:]
            config_path = resource("config", config_name + ".yml")
            args.config = config_file(config_path)
            unknown.pop(0)
        except FileNotFoundError:
            pass

    if unknown:
        parser.error(f"Unrecognized arguments: {unknown}")

    if not args.config:
        parser.error("Config has not been specified")

    return args


def config_file(path):
    file = pathlib.Path(path)
    if file.exists():
        return file
    else:
        raise FileNotFoundError


class WalltimeFormatter(logging.Formatter):
    def format(self, record):  # noqa A003 Function has that name in superclass
        delta = int(record.relativeCreated / 1000)
        seconds = delta % 60
        minutes = int((delta - seconds) / 60) % 60
        walltime = datetime.now().replace(minute=minutes, second=seconds)
        return (
            f"{record.levelname}\t{record.name}"
            f"\t{walltime.strftime('%M:%S')}\t{record.msg}"
        )


def setup_logging(logfile):
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    if not logger.hasHandlers():
        formatter = WalltimeFormatter()

        stderr_handler = logging.StreamHandler()
        stderr_handler.setFormatter(formatter)
        logger.addHandler(stderr_handler)

        file_handler = logging.FileHandler(logfile)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)


def resource(*path_segments):
    return pathlib.Path(__file__).parent.joinpath(*path_segments)


def run_and_observe(cmd, logger):
    process = subprocess.Popen(  # noqa S603: we control cmd completely
        cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
    )
    output = []
    for bline in iter(lambda: process.stdout.readline(), None):
        if not bline:
            break
        line = bline.decode("utf-8")
        logging.getLogger(logger).info(line.strip("\n"))
        output.append(line)
    return output


def run_and_report(cmd):
    return subprocess.run(  # noqa S603: we control cmd completely
        cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
    )


def get_sha256(filename):
    with open(filename, "rb") as f:
        return hashlib.sha256(f.read()).hexdigest()


def create_witness(result, args, property_text):
    with open(resource("template.graphml"), "r") as f:
        template = f.read()
    if result == RESULT_TRUE_PROP:
        template = re.sub(r"\$WITNESSTYPE", "correctness_witness", template)
        template = re.sub(r"\$VIOLATIONNODE", "", template)
    else:
        template = re.sub(r"\$WITNESSTYPE", "violation_witness", template)
        template = re.sub(
            r"\$VIOLATIONNODE", '\n<data key="violation">true</data>', template
        )
    template = re.sub(r"\$PRODUCER", VERSIONSTRING, template)
    template = re.sub(
        r"\$ARCHITECTURE", "32bit" if args.datamodel == "ILP32" else "64bit", template
    )
    template = re.sub(r"\$PROGRAMFILE", args.program, template)
    template = re.sub(r"\$PROGRAMHASH", get_sha256(args.program), template)
    template = re.sub(r"\$SPECIFICATION", property_text, template)
    timestamp = datetime.utcnow().replace(microsecond=0).isoformat() + "Z"
    template = re.sub(r"\$TIMESTAMP", timestamp, template)
    with open(resource(OUT_DIR, "witness.graphml"), "w") as witness_file:
        witness_file.write(template)


def determine_frama_c_version():
    process = subprocess.Popen(  # noqa S603: we control cmd completely
        [FRAMACPATH, "--version"], stdin=subprocess.PIPE, stdout=subprocess.PIPE
    )
    return (
        "".join([bline.decode("utf-8") for bline in process.stdout.readlines()])
        if process.stdout
        else "unknown(no output on version call)"
    )


def get_arch(datamodel):
    if "LP64" in datamodel:
        return "x86_64"
    else:
        return "x86_32"


def get_config(args, property_):
    """This specifies which options to run on a certain property"""

    if args.reach_as_overflow and property_ == Property.UNREACH_CALL:
        property_ = Property.NO_OVERFLOW
    if args.reach_as_acsl and property_ == Property.UNREACH_CALL:
        property_ = Property.VALID_ACSL

    # Parse config file and extract relevant options
    with open(args.config, "r") as f:
        config = yaml.safe_load(f)
    common_config = []
    relevant_subconfig = []
    for subconfig in config:
        if "common" in subconfig:
            common_config.extend(subconfig.get("common"))
        if property_.value in subconfig:
            relevant_subconfig.extend(subconfig.get(property_.value))
    if not relevant_subconfig:
        raise ValueError(f"No configuration specified for property '{property_}'")
    relevant_subconfig.extend(common_config)

    # Split up config options into regular options (additional arguments)
    # and flags (no arguments) for easier access later
    flags = set()
    options = {}
    for config_option in relevant_subconfig:
        if type(config_option) == str:
            flags.add(config_option)
        elif type(config_option) == dict:
            options = {**options, **config_option}
        else:
            raise TypeError(f"Unknown option '{config_option}'")

    # Handle flag "inline-functions"
    inline_options = []
    if "inline-functions" in flags or property_ == Property.UNREACH_CALL:
        inline_functions = get_inline_functions(args.program)
        if inline_functions:
            inline_options = [
                "-inline-calls",
                ",".join(inline_functions),
                "-remove-inlined",
                ",".join(inline_functions),
            ]
        elif inline_functions is None:
            logging.getLogger("WRAPPER").warning(
                "Parsing of program failed, running Frama-C without function inlining"
            )

    # Build Frama-C config
    framac_options = options["frama-c-options"]
    framac_options.extend([*inline_options, "-machdep", get_arch(args.datamodel)])

    # Return Frama-C config and handle option "restart-eva"
    if "restart-eva" in options:
        eva_precisions = options["restart-eva"]
        for precision in eva_precisions:
            yield framac_options + ["-eva-precision", str(precision)]
    else:
        yield framac_options


def run_and_interpret_framac(args, program, property_, report):
    if args.reach_as_overflow:
        output_interpreter = ReportInterpreter(Property.NO_OVERFLOW, report)
    elif args.reach_as_acsl:
        output_interpreter = ReportInterpreter(Property.VALID_ACSL, report)
    else:
        output_interpreter = ReportInterpreter(property_, report)

    result = RESULT_UNKNOWN
    for options in get_config(args, property_):
        cmd = (
            [FRAMACPATH]
            + options
            + [str(program)]
            + [str(resource("harness.c"))]
            # + ["/usr/share/frama-c/libc/stdlib.c"]
            + ["-kernel-warn-key", "CERT:MSC:38=inactive"]
            + ["-then", "-report-csv", str(report)]
        )
        logging.getLogger("WRAPPER").info(
            f"Executing Frama-C with command \"{' '.join(cmd)}\""
        )
        output = run_and_observe(cmd, "FRAMAC")
        result = output_interpreter.interpret(output)
        if result != RESULT_UNKNOWN:
            break

    return result


def instrument_program(args, property_):
    new_text = ""
    if property_ is Property.MEMORY_SAFETY:
        new_text += "#include <stddef.h>\n"
        new_text += "\n"
        new_text += "extern void *__builtin_alloca(size_t);\n"
        new_text += "extern double __builtin_huge_val(void);\n"
        new_text += "extern float __builtin_huge_valf(void);\n"
        new_text += "extern float __builtin_inff(void);\n"
        new_text += "extern float __builtin_nan(const char*);\n"
        new_text += "extern float __builtin_nanf(const char*);\n"
        new_text += "extern void *__builtin_memcpy(void*, const void*, size_t);\n"
        new_text += "extern void *__builtin_memset(void*, int, size_t);\n"
        new_text += "extern void *__builtin_memmove(void*, const void*, size_t);\n"

    with open(args.program, "r") as program:
        new_text += "".join(program.readlines())

    if args.reach_as_overflow:
        new_text = re.sub(
            r"void\s+reach_error\(\s*(void)?\s*\)\s*{",
            "void reach_error() {int __fc_unreach_test = ((int)-2147483648)*-1;",
            new_text,
        )
    elif property_ is Property.UNREACH_CALL and not args.reach_as_acsl:
        new_text = re.sub(
            r"void\s+reach_error",
            "/*@ requires unreach: \\\\false;*/ void reach_error",
            new_text,
        )
    return new_text


def setup_framac():
    """
    Since frama-c still works with hardcoded, absolute paths,
    this method will "install" a version of frama-c in the folder
    framac to FIXEDPATH (usually inside the tmp dir)
    by creating a symlink. This version can be built using
    Vagrant by running `make framac`.
    TODO: this mechanism could be improved with linux namespaces
    and pivot_root (similar to change root)
    """
    if os.path.isdir(resource("framac")):
        logger = logging.getLogger("SETUP")
        logger.info(
            "Frama-C is bundled with Frama-C-SV, setting up this version for usage"
        )
        target = resource("framac").resolve()
        os.environ["PATH"] = "%s:%s" % (
            os.environ["PATH"],
            "/tmp/framac/.opam/framac/bin/",
        )
        if not os.path.islink(FIXEDPATH):
            assert not os.path.isfile(FIXEDPATH)
            os.symlink(target, FIXEDPATH)
        # make sure sticky bit is set on parent dir
        if (
            not (
                os.stat(pathlib.Path(FIXEDPATH).parent.absolute()).st_mode
                & stat.S_ISVTX
                == stat.S_ISVTX
            )
            and not args.unsafe
        ):
            logger.warning("directory for framac has sticky bit not set!")
            sys.exit(1)
        # make sure we are the owner of the link:
        assert os.getuid() == os.stat(FIXEDPATH).st_uid
        global FRAMACPATH
        FRAMACPATH = str(resource("framac/bin/frama-c").absolute())
        logger.info("Bundled FRAMA-C has been setup successfully")
        logger.info(
            "The following version of frama-c located at %s will be used:"
            % (FRAMACPATH)
        )
        run_and_observe([FRAMACPATH, "-version"], "SETUP")
    else:
        logger.info(
            "Frama-C is not bundled with Frama-C-SV, assuming frama-c binary is on $PATH"
        )


if __name__ == "__main__":
    args = parse_args()

    resource(OUT_DIR).mkdir(parents=True, exist_ok=True)

    log_file = resource(OUT_DIR, "frama-c-sv.log")
    log_file.unlink(missing_ok=True)  # pytype: disable=wrong-keyword-args
    setup_logging(log_file)

    setup_framac()

    # dirty hack to get why3 solvers working when benchmarking on vcloud:
    why3_process = run_and_report(["why3", "config", "--detect"])
    if why3_process.returncode != 0:
        # API changed in version 1.4.0 (cf. https://www.mail-archive.com/why3-club@lists.gforge.inria.fr/msg01472.html)
        why3_process = run_and_report(["why3", "config", "detect"])
    output = why3_process.stdout.decode("utf-8")
    for line in output.split("\n"):
        logging.getLogger("WHY3").info(line)

    with open(args.property, "r") as prop_file:
        property_text = prop_file.read()
        property_ = Property.from_text(property_text)

    generated_program_file = resource(OUT_DIR, "program.c")
    report_file = resource(OUT_DIR, "report.csv")
    report_file.unlink(missing_ok=True)  # pytype: disable=wrong-keyword-args

    with open(generated_program_file, "w") as f:
        f.write(instrument_program(args, property_))

    result = run_and_interpret_framac(
        args, generated_program_file, property_, report_file
    )

    create_witness(result, args, property_text)
    logging.getLogger("RESULT").info(result)
