// This file is part of frama-c-sv,
// a wrapper around Frama-C that makes it possible
// to execute Frama-C as part of the Competition on Software Verification:
// https://gitlab.com/sosy-lab/software/frama-c-sv
//
// SPDX-FileCopyrightText: 2020-2021 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

extern int __VERIFIER_nondet_int();
void reach_error(){}

int main() {
  int x = 0;
  int y = 0;
  int i = 0;
  //@ loop invariant x==y;
  while (i<3) {
    // assert x==y;
    x++;
    y++;
    i++;
    // assert x==y;
  }
  if (!(x==y)) reach_error();
  return 0;
}
