// This file is part of frama-c-sv,
// a wrapper around Frama-C that makes it possible
// to execute Frama-C as part of the Competition on Software Verification:
// https://gitlab.com/sosy-lab/software/frama-c-sv
//
// SPDX-FileCopyrightText: 2020-2021 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

void reach_error(){}

int main() {
  int a;
  for(a = 0; a < 6; ++a) {}
  if(!(a == 6 )) {
    reach_error();
  }
  return 1;
}
